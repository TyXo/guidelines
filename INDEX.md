<h1>Development Guidelines</h1>

A general development guidelines for when working with Unity projects.

This repository is meant to be included in the projects either as an sub-module or as a brute copy. This way every developer involved will have quick access to it and know when something changed mid-development - which should be avoided as much as possible but can happen.

Also we use the keywords `Always`, `Consider`, `Avoid` and `Never`. To avoid ambiguity please refer to the table below for the meaning of each word.

| Keyword | Description |
| :--- | :--- |
| `Always` | Means that it is a mandatory rule to follow. |
| `Consider` | Considered a good practice but without a real major effect on the project. |
| `Avoid` | Considered a bad practice most of the time due the side effects so always ask the lead programmer about those.
| `Never` | Means that it is a mandatory rule to follow. |

>**Warning:** different projects may have different guidelines due to many reasons so always ask for the lead programmer before starting to follow blindly what is described here.

- [Team Guidelines](WikiContent/Team-Guidelines.md)
- [Programming Guidelines](WikiContent/Programming-Guidelines.md)
- [Technology Definition](WikiContent/Technology-Definition.md)
- [Good Practices](WikiContent/Good-Practices.md)