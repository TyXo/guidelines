<h1>Team Guidelines</h1>

This document describes guidelines for everyone involved on the project - including programmers, artists, testers, managers and so on.

- [Discord](#discord)
  - [Always use the project specific channel/server to talk about the project](#always-use-the-project-specific-channelserver-to-talk-about-the-project)
  - [Always be objective when mentioning someone or sending private messages due some emergency](#always-be-objective-when-mentioning-someone-or-sending-private-messages-due-some-emergency)
  - [Consider moving off-topic conversation to private or another channel](#consider-moving-off-topic-conversation-to-private-or-another-channel)
  - [Avoid mentioning `@everyone` and `@here`](#avoid-mentioning-everyone-and-here)
  - [Avoid using other side-groups to talk about the project](#avoid-using-other-side-groups-to-talk-about-the-project)
- [File Sharing](#file-sharing)
  - [Always use the Unity Cloud Build for sharing builds with the team](#always-use-the-unity-cloud-build-for-sharing-builds-with-the-team)
  - [Always use `zip` when compressing files](#always-use-zip-when-compressing-files)
  - [Always use `mp4` for video sharing](#always-use-mp4-for-video-sharing)
- [Trello](#trello)
  - [Bug reporting](#bug-reporting)
  - [Always keep Trello board up to date](#always-keep-trello-board-up-to-date)
  - [Always split bigger tasks into smaller ones](#always-split-bigger-tasks-into-smaller-ones)
  - [Always add a flag and move it back to `To Do` when re-opening an issue](#always-add-a-flag-and-move-it-back-to-to-do-when-re-opening-an-issue)
  - [Always add a flag and leave it `In Progress` when you need to work on an blocked issue](#always-add-a-flag-and-leave-it-in-progress-when-you-need-to-work-on-an-blocked-issue)
  - [Consider re-estimating sooner than later](#consider-re-estimating-sooner-than-later)
  - [Consider adding a screenshot or video in the card before sending to review in visual-related issues](#consider-adding-a-screenshot-or-video-in-the-card-before-sending-to-review-in-visual-related-issues)
- [Unity Project](#unity-project)
  - [Assets](#assets)
  - [Always create a folder named `_Project` in the `Assets` folder root to store non-third-party content](#always-create-a-folder-named-_project-in-the-assets-folder-root-to-store-non-third-party-content)
  - [Always keep the `_Project` folder organized](#always-keep-the-_project-folder-organized)
  - [Always leave third-party assets untouched when imported](#always-leave-third-party-assets-untouched-when-imported)
  - [Always delete unused content from the project](#always-delete-unused-content-from-the-project)
  - [Always use real world scaling unless the project requires otherwise](#always-use-real-world-scaling-unless-the-project-requires-otherwise)
  - [Always reset an empty GameObject position, rotation and scale](#always-reset-an-empty-gameobject-position-rotation-and-scale)
  - [Always create prefabs for the models and use them instead](#always-create-prefabs-for-the-models-and-use-them-instead)
- [Version Control](#version-control)
  - [General](#general)
  - [Merges](#merges)
  - [Branches](#branches)
  - [Commits](#commits)

---
# Discord

## Always use the project specific channel/server to talk about the project
You should always ask for advice and guidance when you feel that you need it - we are a team after all! Just remember to use the project channels for that instead of using private messages.

Being stuck on some problem and asking on the project channel can reward you with a much faster message, as someone that you didn't expect could have the right answer for you.

You should only use private messages for personal stuff, for sensitive data (like accounts and password) or in real emergencies.

## Always be objective when mentioning someone or sending private messages due some emergency
You will save time for both sides if you upfront all necessary info that the person may need to help you.

## Consider moving off-topic conversation to private or another channel
Everyone loves funny glitch/bug and its okay to share those in the project channel. Just remember that we should not extend too much those off-topic conversations as this will cause the channel history to become a mess.

## Avoid mentioning `@everyone` and `@here`
Mention specific roles and/or people of interest instead. There is a lot of people on the project groups - many just as support or watcher - so avoid being the one to spam messages on everyone's notification system.

Also, it is worth mentioning that `@here` and `@everyone` have a subtle difference. `@here` notifies all non-idle users currently online and `@everyone` notifies all users - even if they are offline.

## Avoid using other side-groups to talk about the project
Sometimes we need to create groups to be able to share the screen and thats okay. But remember to always comeback to the main project channel/server so that the entire team will be aware of what is happening.

---
# File Sharing

## Always use the Unity Cloud Build for sharing builds with the team
You can upload local builds there too and all versions will be on the same place.

## Always use `zip` when compressing files
Some formats aren't handled well between different operational system (like the commonly used `rar` format) so you should always use the `zip` one.

## Always use `mp4` for video sharing
It generates a nice preview on the common platforms and is widely accepted on most video players.

---
# Trello

## Bug reporting
Always report the bugs found both on Trello and on Discord (in that order). The bug reporting should contain all relevant info about it. Here is a small template to use across the project (which may be adapted according to the project):

**Issue:** insert the issue link here in the format `<`link`>` to not generate a preview (fill that field only on Discord, ignore it on Trello)<br/>
**Version:** the full version number with the link to download the version or the commit hash<br/>
**Platform:**: like "Windows / i5-4440 / GTX 1050ti / 8GB Ram" or "Android 9 / Samsung A6+"<br/>
**Expected result:**: if you don't know be clear about that<br/>
**Actual result:**: what did you see? It is good to also add a screenshot or video here too<br/>
**How to reproduce:**: if not consistent be clear about that

For new issues create it on backlog and fill the description with the information above. For issues on review add a comment with that information and move it back to `To Do` with a flag.

## Always keep Trello board up to date
Avoid misunderstanding of the current state of the project for your team-mates and the project manager.

## Always split bigger tasks into smaller ones
Most clients likes to check the Hubstaff report and breaking the tasks into smaller ones already avoids most misunderstandings about the durability of specific tasks.

## Always add a flag and move it back to `To Do` when re-opening an issue
This will help to quickly identify that this issue was a regression or didn't pass the review. The developer should then remove the flag when will start to work on it.

## Always add a flag and leave it `In Progress` when you need to work on an blocked issue
This will help to quickly identify that we need re-organize the priorities between the tasks. Once unblocked don't forget to remove the flag.

## Consider re-estimating sooner than later
This will help the project manager to re-plan the sprint if necessary.

## Consider adding a screenshot or video in the card before sending to review in visual-related issues
This will improve the review speed and gathering of feedback for it.

---
# Unity Project

## Assets
- Always use `PascalCase_UnderscoreTolerant` format when naming assets. You can use `_` to define variations of the same asset, for example:
  ```
  PlayerWeapon_001
  PlayerWeapon_002
  SoldierGloves_Red
  SoldierGloves_Blue
  LevelFloor_Metal
  LevelFloor_Lava_001
  LevelFloor_Lava_002
  ```
- Always use the format `Context@Animation` for animation clips and animation-only FBXs, for example:
  ```
  Player@Idle_001
  Player@Idle_002
  Ghost@Moving
  ```
- Avoid using numbers to define variations, but if you do ensure to always use 3 digits for that.
- Never add the type of the asset in the name, Unity has a built-in way to filter assets by type (like searching for "player t:model" will only list model assets with the word "player" on it).

## Always create a folder named `_Project` in the `Assets` folder root to store non-third-party content
It should be literally `_Project`, and inside of it create sub-folders like `Audio`, `Scenes`, `Scripts` and so on.

## Always keep the `_Project` folder organized
As a tip here: folder and files that begins with "__" (double underscore) are ignored on our default `.gitignore`, so consider putting temporary stuff somewhere like "__Temp".

## Always leave third-party assets untouched when imported
Do not move things around neither modify the assets directly (you may create a copy of some asset to modify your own). This way we will avoid issues when trying to upgrade those.

You may create a folder named `Plugins` in the root of the `Assets` to put the third-party content there, but be careful to check if the imported asset is compatible with that.

Also, if it contains a `package.json` file on the root you may move it to the Packages folder. This should be considered over the other options.

## Always delete unused content from the project
If needed we can later restore things re-importing determined packaged or through the version control.

## Always use real world scaling unless the project requires otherwise
You should adjust the model scale in the model importer to achieve that or in the source file itself. Change the transform scale only if really needed.

## Always reset an empty GameObject position, rotation and scale
Leaving things out of the center will cause unnecessary confusion when moving objects around.

## Always create prefabs for the models and use them instead
This will make easier to adjust position, replace models and create prefab variants out of that model in the future.

---
# Version Control

## General
- Always use GitLab for new projects by default.
- Always use our default `.gitignore` file provided [here](.gitignore).
- Always initialize the Git LFS. Use the `.gitattributes` provided [here](.gitattributes).
- Always use text serialization and visible meta files in `Edit/Project Settings.../Editor`.
- Consider pulling the latest modifications often to avoid conflicts.
- Consider using TortoiseGit on Windows and SourceTree on Mac.
- Avoid using a different git client without talking with the lead programmer first.
- Avoid using HTTPS connection, setup a SSH connection as soon as possible.

## Merges
- Always leave the default merge message as it tell us which files had conflicts.
- Always keep the origin (repository) version in conflicts and re-do your modifications. In case of doubt always consult the lead programmer about that.

## Branches
- Always keep the master branch at a minimum stable state (can play in editor and play the build).
- Always keep your working branch updated with the master branch latest changes to reduce conflicts.
- Always delete the branch after merging it back to the master if the work on it has finished.
- Always use the format `type-nameofbranch` in all-lower case for naming the branch, for example:
  ```
  feature-playercontroller
  update-tankphysics
  fix-missingmobileshaders
  release-1.1.0
  ```

## Commits
- Always review the files included in the commit, you should not send undesired changes on them. A good example is changing to show the Unity logo on the splash screen, which Unity automatically changes that if you are not a subscriber, but you can always open the file and revert that specific line.
- Always use imperative form in your commits, leave past form for end-user change logs.
- Always put one modification per line on the message with a `;` at the end of each line (may skip it on the last one).
- Always use unix line endings when doing the commit.
- Consider putting the Trello card name.
- Consider to commit often, having multiple smaller commits are easier and faster to track down issues later. 

Example commit message:
```
Add VR Support (ID-120);
Change default speed to 10km/h (ID-150);
Decrease max speed to 120km/h (ID-200);
Fix player getting stuck on the side of the road sometimes;
Update version to 1.2.4
```
