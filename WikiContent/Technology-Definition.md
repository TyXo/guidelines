## Unity Version
Unity 2019.3

## Render Pipeline
HDRP 7 - Clustered Tiled Forward Renderer

## Networking
Mirror (Always use latest asset store version!)
Steam Transport (FizzySteamy)

## Input System
New Unity Input System

## User Data Management
PlayFab
